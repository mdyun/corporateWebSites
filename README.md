#企业官网

## 项目描述

一套展示性的企业官方网站，包含了一般网站常用的特效，主要为了深入巩固自己的页面布局思想。

## 技术点

1. 使用HTML5技术
2. 在页面中使用CSS3制作页面动画效果
3. 原生JavaScript写的轮播效果
4. 使用CSS预处理器，Sass

## 在线DEMO地址

[http://xzcgg.gitee.io/corporatewebsites](http://xzcgg.gitee.io/corporatewebsites)

##  项目收获

前期用css+js写完了整个网站，后期学习CSS预处理器Sass，然后用Sass重构了项目。强化了自己的页面布局思路，以及对Sass使用的熟练程度。

## 项目截图


![Paste_Image.png](http://upload-images.jianshu.io/upload_images/3152923-75659c7bf4379352.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)



![Paste_Image.png](http://upload-images.jianshu.io/upload_images/3152923-2e8f74cc5a6c6431.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)




![Paste_Image.png](http://upload-images.jianshu.io/upload_images/3152923-88909896ad11eadd.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)



![Paste_Image.png](http://upload-images.jianshu.io/upload_images/3152923-82a6ad69ce2b1d8f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)





